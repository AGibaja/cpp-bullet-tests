/*!
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/


/*!
	@class EEntity
	@brief Generic Entity used for other elements construction.

*/


#ifndef EENTITY_HPP
#define EENTITY_HPP

#include <string>

#include <btBulletDynamicsCommon.h>
#include <Updateable.hpp>
#include <Model_Obj.hpp>

using  ModelObj = glt::Model_Obj;

class ERigidBody;

class EShape;

class EEntity : public Updateable
{
public:
	///Constructor of the catapult from a position, scale and rotation.
	/*!
	 * @param position origin of the catapult.
	 * @param scale scale of the catapult,
	 * @param rotation rotation of the catapult.
	 */
	EEntity(btVector3 position = btVector3(0,0,0), 
		btVector3 scale = btVector3(1,1,1), 
		btVector3 rotation = btVector3(0,0,0));


	///Constructor of the catapult from a rigidbody, and scale.
	/*!
	 * @param model path of the 3D model of the entity.
	 * @param rb rigidbody for the entity.
	 * @param scale scale of the catapult.
	 */
	EEntity(std::string path, std::shared_ptr<ERigidBody> rb, btVector3 scale);


	///Update the entity as desired.
	void update() override;


	///Get the path for this model.
	std::string getPath(); 


	///Get the scale for this entity.
	btVector3 getScale(); 


	///Get the physics transform for this entity.
	btTransform getPTransform();


	///Get own abstraction of the rigidbody for this entity.
	std::shared_ptr<ERigidBody> getRb();


	///Get the render node for this entity.
	std::shared_ptr<ModelObj> getGraphics(); 


protected:
	///Path for the entitys model.
	std::string path;

	///Position of the entity.
	btVector3 position;
	///Scale of the entity.
	btVector3 scale;
	///Rotation of the entity.
	btVector3 rotation;

	///Bullet transform of the entity.
	btTransform p_transform; 


	///Rigid body own implementation pointer for this entity.
	std::shared_ptr<ERigidBody> rigid_body = nullptr;

	///Own implementation of shapes for this body.
	std::shared_ptr<EShape> shape = nullptr;

	///Graphics for the entity (opengl toolkit nodes)
	std::shared_ptr<ModelObj> graphics = nullptr;

	///Transformation matrix for graphics.
	glm::mat4 graphics_transform;

};




#endif // !#define EENTITY_HPP
