/*!
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

/*!
	@class EShape
	@brief Base class for own encapsulation of bullets shapes.
*/

#include <btBulletDynamicsCommon.h>

#include <memory>


class EShape
{
public:
	///Enum naming own types.
	enum SHAPE_TYPE
	{
		UNDEFINED = 0,
		BOX,
		SPHERE,
		CYLINDER
	} shape_type;

	

	///Getter for the collision shape of bullet.
	std::shared_ptr<btCollisionShape> getBtShape();


protected:
	///Bullet shape that this encapsulates.
	std::shared_ptr<btCollisionShape> bt_shape;
	

};