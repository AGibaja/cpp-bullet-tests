#include <EWheel.hpp>
#include <EBox.hpp>
#include <ECatapult.hpp>
#include <EDRigidBody.hpp>



EWheel::EWheel(ECatapult * parent_catapult, btVector3 position, btVector3 scale, btVector3 rotation)
	: EEntity(position, scale, rotation)
{
	this->graphics = std::make_shared<ModelObj>("../../../../assets/cylinder.obj");

	this->shape = std::make_shared<EBox>(this->scale*0.5f);
	this->rigid_body = std::make_shared<EDRigidBody>(this->shape, 0.1, position, rotation);

}
