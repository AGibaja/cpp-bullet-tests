/*!
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

/*!
	@class EDRigidBody
	@brief Abstraction of a a bullet dynamic rigidbody into own class. The D on EDPlatform
		stands for dynamic.
*/



#ifndef EDRIGIDBODY_HPP
#define EDRIGIDBODY_HPP

#include <ERigidBody.hpp>


using BConstructionInfo = btRigidBody::btRigidBodyConstructionInfo;

class EDRigidBody : public ERigidBody
{
public:
	///Constructor that takes the shape of a bullet rigidbody, its position and rotation.
	/*!
	 * This class construct a dynamic rigidbody given a shape, mass, position and rotation.
	 * @param shape shape of the rigidbody.
	 * @param mass mass of the rigidbody.
	 * @param position origin of the rigidbodys transform.
	 * @param rotation rotation of the rigidbodys transform.
	 */
	EDRigidBody(std::shared_ptr<EShape> shape, float mass, btVector3 position, btVector3 rotation);
	
	
	///Destructor of the dynamic.
	~EDRigidBody();


protected:
	///Initializes the rigidbodies properties as needed.
	/*!
	 * Initializes the bullet rigidbody from its shape and mass.
	 */
	void initBody() override;
	///Mass of the dynamic rigid body.
	float mass;


};

#endif // !EDRIGIDBODY_HPP


