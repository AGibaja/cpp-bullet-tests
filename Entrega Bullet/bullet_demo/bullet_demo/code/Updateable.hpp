/**
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

/*!
	@brief This class implements specific behaviours fro classes that can be updated.
*/


#ifndef UPDATEABLE_HPP
#define UPDATEABLE_HPP


class Updateable
{
	///What to do on update.
	virtual void update() = 0;
};

#endif // !define UPDATEABLE_HPP
