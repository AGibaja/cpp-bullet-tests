#include "ERigidBody.hpp"



void ERigidBody::initBody()
{

}


ERigidBody::ERigidBody(std::shared_ptr<EShape> & shape, btVector3 position, btVector3 rotation)
	: shape(shape)
{
	btTransform tr; 
	tr.setIdentity();
	tr.setOrigin(position);

	btQuaternion quat;
	quat.setEuler(btRadians(rotation.getX()), btRadians(rotation.getY()), btRadians(rotation.getZ()));
	tr.setRotation(quat);

	this->motion_state = std::make_unique<btDefaultMotionState>(tr);
}


btRigidBody *ERigidBody::getBtRigidBody()
{
	return this->rigid_body;
}


std::shared_ptr<EShape> ERigidBody::getEShape()
{
	return this->shape;
}


