/**
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

/*!
	@class EWheel
	@brief This class abstracts the behaviour of the catapults wheel.
*/

#ifndef EWHEEL_HPP
#define EWHEEL_HPP

#include <EEntity.hpp>


class ECatapult;

class EWheel : public EEntity
{
public:

	///Constructor that forms the wheel. Defines its basics properties.
	/*!
		@param parent_catapult catapult that it belogns to.
		@param position of the wheel, both graphics and physics position.
	 	@param scale of the wheel, both graphics and physics position.
	 	@param rotation of the wheel, both graphics and physics position.
	 */
	EWheel(ECatapult *parent_catapult,
		btVector3 position = btVector3(0, 0, -10),
		btVector3 scale = btVector3(1, 1, 1),
		btVector3 rotation = btVector3(0, 0, 0));


private:

	///Catapult that the wheel forms part of.
	std::shared_ptr<ECatapult> catapult_parent = nullptr;


};

#endif // !EWHEEL_HPP


