/*!
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

/*!
	@class ERigidbody
	@brief Base class for all rigidbodies.
*/

#ifndef ERIGIDBODY_HPP
#define ERIGIDBODY_HPP

#include <btBulletDynamicsCommon.h>
#include <memory>

class EShape;

class ERigidBody
{
public:
	
	///Constructor of the rigidbody.
	/*!
	 * @param shape shape to assign to this rigidbody.
	 * @param position position of the rigidbodies transform.
	 * @param rotation rotation of the rigidbody.
	 */
	ERigidBody(std::shared_ptr<EShape> & shape, btVector3 position, btVector3 rotation);
	

	///Get bullets rigidbody.
	btRigidBody *getBtRigidBody();


	///Get shape assigned to this rigidbody.
	std::shared_ptr<EShape> getEShape();


protected:
	///Shape of the body.
	std::shared_ptr<EShape> shape;

	///Bullet rigidbody.
	btRigidBody* rigid_body;

	///Motion state of the rigidbody, usefull for collisions.
	std::unique_ptr<btDefaultMotionState> motion_state;

	///Virtual function that defines a common initialization function for each rigidbody.
	virtual void initBody() = 0;

};

#endif