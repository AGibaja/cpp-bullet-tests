/*!
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

/*!
	@class ESphere
	@brief Abstraction of bullets sphere shape into own class.
*/



#ifndef ESPHERE_HPP
#define ESPHERE_HPP

#include <EShape.hpp>


class ESphere : public EShape
{
public:
	///Constructor that takes the spheres dimensions.
	ESphere(float radius);

	///Getter for the spheres radius.
	float getRadius(); 


private:
	///Radius of the sphere.
	float radius;

};

#endif // !#define ESPERE_HPP
