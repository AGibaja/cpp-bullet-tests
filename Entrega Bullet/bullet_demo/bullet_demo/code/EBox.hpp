/*!
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/
/*!
	@class EBox
	@brief Abstraction of bullets box shape into own class.
*/


#ifndef EBOX_HPP
#define EBOX_HPP

#include <EShape.hpp>
#include <btBulletDynamicsCommon.h>


class EBox : public EShape
{
public:
	///Constructor that takes the box dimensions as half extents.
	EBox(btVector3 dimensions);


	///Getter for the box dimensions.
	btVector3 getDimensions();


private:
	///Dimensions of the box.
	btVector3 dimensions;

};

#endif // !define EBOX_HPP
