#include <ECylinder.hpp>

ECylinder::ECylinder(btVector3 dimensions)
	: dimensions(dimensions)
{
	this->bt_shape = std::make_shared<btCylinderShape>(dimensions);
	this->shape_type = CYLINDER;
}

btVector3 ECylinder::getDimensions()
{
	return this->dimensions;
}
