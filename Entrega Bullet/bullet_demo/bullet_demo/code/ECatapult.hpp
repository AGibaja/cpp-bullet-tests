/**
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/


/*!
	@class ECatapult
	@brief Represents a catapult with its wheels.
	The catapult is a rigidbody with two hinge constraints (one per wheel), being both wheels pivot points
	the catapults body. It uses motors to move the wheels and the catapult.
*/

#ifndef CATAPULT_HPP
#define CATAPULT_HPP

#include <EEntity.hpp>


class EDRigidBody;
class EScene; 
class EWheel;

class ECatapult : public EEntity 
{
public:
	///Constructor of the catapult from a position, scale and rotation.
	/*!
	 * @param s Scene to add catapults wheels to. 
	 * @param position Origin of the catapult. 
	 * @param scale Scale of the catapult,
	 * @param rotation Rotation of the catapult.
	 */
	ECatapult(EScene *s, btVector3 position = btVector3(0, 0, -10), btVector3 scale = btVector3(1, 1, 1),
		btVector3 rotation  = btVector3(0, 0, 0));

	///Destructor for the catapult.
	~ECatapult();

	///Add left wheel to the physics and graphics world with its hinges.
	/*!
	 * Add the left wheel to the catapult. Adding the wheel to the graphics world, physics world
	 * and the EScene is handled int this method.
	 * @param wheel Own encapsulation of a wheel from where to get and create all things needed.
	 */
	void addLeftWheel(std::shared_ptr<EWheel> wheel);
	
	
	///Add right wheel to the physics and graphics world with its hinges.
	/*!
	 * Add the right wheel to the catapult. Adding the wheel to the graphics world, physics world
	 * and the EScene is handled int this method.
	 * @param wheel own encapsulation of a wheel from where to get and create all things needed.
	 */
	void addRightWheel(std::shared_ptr<EWheel> wheel);
	
	
	///Turn the whole catapult left.
	/*!
	 * Turns the whole catapult left applying angular velocities with opposed directions
	 * per wheel.
	 */
	void turnLeft();
	
	
	///Turn the whole catapult right.
	/*!
	 * Turns the whole catapult right applying angular velocities with opposed directions
	 * per wheel.
	 */
	void turnRight();


	///Move the catapult forward. 
	/*!
	 * Move the whole catapult forward applying forces in the same direction (positive) per wheel.
	 */
	void forward();


	///Move the catapult backwards. 
	/*!
	 * Move the whole catapult backwardsapplying forces in the same direction (negative) per wheel.
	 */
	void backwards();


	///Getter for the hing constraint of the left wheel.
	std::shared_ptr<btHingeConstraint> getLeftWheel();


	///Getter for the hing constraint of the right wheel.
	std::shared_ptr<btHingeConstraint> getRightWheel();


private:
	///Scene to where the catapult belongs.
	EScene *s = nullptr;

	///List of wheels that form the catapult.
	std::vector<std::shared_ptr<EWheel>> wheels;
	///Pointer to the left wheel constraint.
	std::shared_ptr<btHingeConstraint> left_wheel_constraint = nullptr;
	///Pointer to the right wheel constraint.
	std::shared_ptr<btHingeConstraint> right_wheel_constraint = nullptr;
};




#endif // !CATAPULT_HPP
