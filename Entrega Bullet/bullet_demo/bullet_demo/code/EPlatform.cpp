#include <EPlatform.hpp>
#include <EBox.hpp>
#include <ESRigidBody.hpp>



EPlatform::EPlatform(btVector3 position, btVector3 scale, btVector3 rotation)
	: EEntity(position, scale, rotation)
{
	this->initPlatform(); 
}


void EPlatform::initPlatform()
{
	this->graphics = std::make_shared<ModelObj>("../../../../assets/box.obj");
	this->shape = std::make_shared<EBox>(this->scale * 0.5f);
	this->rigid_body = std::make_shared<ESRigidBody>(this->shape, this->position, this->rotation);
}
