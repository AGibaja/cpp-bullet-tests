#include <EPhysicsWorld.hpp>



EPhysicsWorld::EPhysicsWorld(btVector3 gravity)
	: gravity(gravity), collision_dispatcher(&collision_configuration)
{
	this->initBulletWorld();
}




EPhysicsWorld::~EPhysicsWorld()
{
	this->bullet_world.reset();	
}


btVector3 EPhysicsWorld::getGravity()
{
	return this->gravity;
}


btDiscreteDynamicsWorld* EPhysicsWorld::getBulletWorld()
{
	return this->bullet_world.get();
}


void EPhysicsWorld::initBulletWorld()
{
	
	this->bullet_world.reset( new btDiscreteDynamicsWorld
		( &collision_dispatcher, &broad_phase, &constraint_solver, &collision_configuration));

	this->bullet_world->setGravity(this->gravity);
}
