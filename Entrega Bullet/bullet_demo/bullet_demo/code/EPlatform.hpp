/**
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

/*!
	@class EPlatform
	@brief Encapsulation of the base behaviour for a platform.
*/

#ifndef EPLATFORM
#define EPLATFORM

#include <EEntity.hpp>


class EScene;


class EPlatform : public EEntity
{
public:	
	///Initialize a platform with some attributes to define where it exists in the scene.
	/*!
		@param position Position of the platform in the scene.
		@param scale Scale of the platform.
		@param rotation Rotation of the platform.
	*/
	EPlatform(btVector3 position = btVector3(0, 5.f, -15.f), btVector3 scale = btVector3(10.f, 1, 10),
		btVector3 rotation = btVector3(0, 90, 0));
	

protected:
	///Initialize the platform accordingly.
	void initPlatform(); 

};

#endif // !EPLATFORM
