#include "EDPlatform.hpp"
#include "EBox.hpp"
#include "ESRigidBody.hpp"



EDPlatform::EDPlatform(btVector3 position, btVector3 scale, btVector3 rotation)
	: EPlatform(position, scale, rotation)
{
	this->initDPlatform(); 
	this->getRb()->getBtRigidBody()->setActivationState(DISABLE_DEACTIVATION);
}


void EDPlatform::isMoving(bool is_moving)
{
	this->is_moving = is_moving;
}


void EDPlatform::movePlatform()
{
	if (this->is_moving)
	{
		this->getRb()->getBtRigidBody()->setLinearVelocity(btVector3(0, 0, -5));
	}
	
}


void EDPlatform::stopMovement()
{
	this->getRb()->getBtRigidBody()->setLinearVelocity(btVector3(0, 0, 0));
}


bool EDPlatform::isMoving()
{
	return this->is_moving;
}


void EDPlatform::initDPlatform()
{
	this->graphics = std::make_shared<ModelObj>("../../../../assets/box.obj");
	this->shape = std::make_shared<EBox>(this->scale * 0.5f);
	this->rigid_body = std::make_shared<EDRigidBody>(this->shape, 50.f,this->position, this->rotation);
	this->getRb()->getBtRigidBody()->setLinearFactor(btVector3(0, 0, 1));
	this->getRb()->getBtRigidBody()->setAngularFactor(btVector3(0, 0, 0));
	this->getRb()->getBtRigidBody()->setFriction(10.f);

}
