#include "EKey.hpp"
#include <EBox.hpp>
#include <ESRigidBody.hpp>



EKey::EKey(btVector3 position, btVector3 scale, btVector3 rotation)
	: EEntity(position, scale, rotation)
{
	this->initKey(); 
}


void EKey::initKey()
{
	this->graphics = std::make_shared<ModelObj>("../../../../assets/sphere.obj");
	this->shape = std::make_shared<EBox>(this->scale * 0.5f);
	this->rigid_body = std::make_shared<ESRigidBody>(this->shape, this->position, this->rotation);
	this->rigid_body->getBtRigidBody()->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);
	this->rigid_body->getBtRigidBody()->setActivationState(DISABLE_DEACTIVATION);

}
