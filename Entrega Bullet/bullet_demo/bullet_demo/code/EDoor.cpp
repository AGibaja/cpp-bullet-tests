#include "EDoor.hpp"
#include <EBox.hpp>
#include <ESRigidBody.hpp>



EDoor::EDoor(btVector3 position, btVector3 scale, btVector3 rotation)
	: EEntity(position, scale, rotation)
{
	this->initDoor();
}


void EDoor::openDoor()
{
	if (!this->is_open)
	{
		btTransform new_t;
		this->getRb()->getBtRigidBody()->getMotionState()->getWorldTransform(new_t);
		this->getRb()->getBtRigidBody()->setActivationState(DISABLE_DEACTIVATION);
		new_t.getOrigin() -= btVector3(0, -5, 0);
		this->getRb()->getBtRigidBody()->getMotionState()->setWorldTransform(new_t);
		this->getRb()->getBtRigidBody()->setWorldTransform(new_t);
		is_open = true;
	}
	
}


void EDoor::initDoor()
{
	this->graphics = std::make_shared<ModelObj>("../../../../assets/box.obj");
	this->shape = std::make_shared<EBox>(this->scale * 0.5f);
	this->rigid_body = std::make_shared<ESRigidBody>(this->shape, this->position, this->rotation);
}
