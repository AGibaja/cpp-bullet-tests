/**
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

///Represents a key that raises the door of the scene.
/*!
	@class EKey
	@details The key is created as a rigidbody that doesn't respond to any other rigidbodies. 
		On collision with the catapults body, (see EScene class ), it wil disappear and raise
		the door in front of it.
*/


#ifndef EKEY_HPP
#define EKEY_HPP

#include <EEntity.hpp>

class EScene;

class EKey : public EEntity
{
public:
	///Constructor of the catapult from a position, scale and rotation.
	/*!
	 * @param origin origin of the key.
	 * @param scale scale of the key.
	 * @param rotation rotation of the key.
	 */
	EKey( btVector3 position = btVector3(0, 0, -10), btVector3 scale = btVector3(5, 5, 5),
		btVector3 rotation = btVector3(0, 0, 0));

private:
	
	///Initializes the entities properties as needed.
	/*!
	 * Initializes the keys graphics, physics (rigidbodies) and shapes.
	 */
	void initKey(); 


};
#endif // !EKEY_HPP
