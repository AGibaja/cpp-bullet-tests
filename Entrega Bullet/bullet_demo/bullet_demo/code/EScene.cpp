#include <EScene.hpp>
#include <Model_Obj.hpp>
#include <ERigidBody.hpp>
#include <ECatapult.hpp>
#include <EKey.hpp>
#include <EDoor.hpp>
#include <EDPlatform.hpp>



EScene::EScene()
{
	this->initScene(); 
	this->setSceneElements();
}


EScene::EScene(std::shared_ptr<EPhysicsWorld> p_world, std::shared_ptr<RenderNode> g_world)
	: physics_world(p_world), graphics_world (g_world)
{
	this->initScene();
	this->setSceneElements();
}


EScene::EScene(std::shared_ptr<EPhysicsWorld> p_world,
	std::shared_ptr<RenderNode>  g_world,
	std::vector<std::shared_ptr<EEntity>> entities)
{
	this->initScene(); 
	this->setSceneElements(); 
}


void EScene::update()
{
	this->physics_world->getBulletWorld()->stepSimulation(1.f / 60.0f);
	this->checkCollisions();
	for (auto &e : this->entities)
	{
		e->update();
	}
}


void EScene::render()
{
	this->getGraphicsWorld()->render();
}


void EScene::setGraphicsScene(std::shared_ptr<RenderNode> graphics_world)
{
	this->graphics_world = graphics_world; 
}


void EScene::setEntities(std::vector<std::shared_ptr<EEntity>> entities)
{
	this->entities = entities;
}


void EScene::setCamera(glt::Vector3 position, glt::Vector3 rotation)
{
	this->camera = std::make_shared<glt::Camera>(); 
	camera->translate(position); 
	camera->rotate_around_x(rotation.x); 
	camera->rotate_around_y(rotation.y); 
	camera->rotate_around_z(rotation.z); 
}


void EScene::setCamera(std::shared_ptr<glt::Camera> camera)
{
	this->camera = camera;
}


void EScene::setLight(std::shared_ptr<glt::Light> light)
{
	this->light = light; 
}


void EScene::addEntity(std::string name ,std::shared_ptr<EEntity> entity)
{
	auto a = entity->getRb()->getBtRigidBody();
	this->physics_world->getBulletWorld()->addRigidBody(a);
	this->graphics_world->add(name, entity->getGraphics());
	this->entities.push_back(entity);

}


void EScene::setCatapult(std::shared_ptr<ECatapult> c)
{
	this->catapult = c;
}


void EScene::setKey(std::shared_ptr<EKey> c)
{
	this->key = c;
}


void EScene::setDoor(std::shared_ptr<EDoor> door)
{
	this->door = door; 
}


void EScene::setMovablePlatform(std::shared_ptr<EDPlatform> platform)
{
	this->movable_platform = platform;
}


void EScene::setDestinationFloor(std::shared_ptr<EPlatform> destiny)
{
	this->destination_floor = destiny;
}


std::shared_ptr<glt::Camera> EScene::getCamera()
{
	return this->camera;
}


std::shared_ptr<glt::Light> EScene::getLight()
{
	return this->light;
}


std::shared_ptr<EPhysicsWorld> EScene::getPhysicsWorld()
{
	return this->physics_world;
}


std::shared_ptr<RenderNode> EScene::getGraphicsWorld()
{
	return this->graphics_world;
}


std::shared_ptr<ECatapult> EScene::getCatapult()
{
	return this->catapult;
}


std::shared_ptr<EDPlatform> EScene::getMovablePlatform()
{
	return this->movable_platform;
}


std::shared_ptr<EPlatform> EScene::getDestinationPlatform()
{
	return this->destination_floor;
}


std::shared_ptr<EDoor> EScene::getDoor()
{
	return this->door;
}


std::vector<std::shared_ptr<EEntity>> EScene::getEntities()
{
	return this->entities;
}


void EScene::initScene()
{
	this->physics_world = std::make_shared<EPhysicsWorld>(btVector3(0, -10, 0));
	this->graphics_world = std::make_shared<RenderNode>();
}

//Add some basic things to the scene.
void EScene::setSceneElements()
{
	this->camera = std::make_shared<glt::Camera>(20.f, 1.f, 200.f, 1.f);
	this->light = std::make_shared<glt::Light>();

	this->graphics_world->add("Camera", this->camera);
	this->graphics_world->add("Light", this->light);

	auto a = *this->graphics_world;
	a["Light"]->translate(glt::Vector3(-5, -5, -5));
	a["Camera"]->translate(glt::Vector3(0, 4, 10));
}

#include <iostream>


void EScene::checkCollisions()
{
	int num_manifolds = this->physics_world->getBulletWorld()->getDispatcher()->getNumManifolds(); 

	for (int i = 0; i < num_manifolds; ++i)
	{
		btPersistentManifold *actual_manifold = this->physics_world->getBulletWorld()->getDispatcher()->getManifoldByIndexInternal(i);

		btCollisionObject *body_0 = (btCollisionObject*)(actual_manifold->getBody0());
		btCollisionObject *body_1 = (btCollisionObject*)(actual_manifold->getBody1());
		
		for (int c = 0; c < actual_manifold->getNumContacts(); ++c)
		{
			btManifoldPoint &p = actual_manifold->getContactPoint(c);

			if (p.getDistance() < 0.f)
			{
				//Collision between key and catapult.
				btRigidBody * key = this->key->getRb()->getBtRigidBody();
				btRigidBody * catapult = this->catapult->getRb()->getBtRigidBody();

				if (body_0 == key && body_1 == catapult || body_1 == key && body_0 == catapult)
				{
					this->key->getGraphics()->set_visible(false);
					this->door->openDoor(); 
				}

				btRigidBody *left_wheel = &this->getCatapult()->getLeftWheel()->getRigidBodyB();
				btRigidBody *m_plat = this->getMovablePlatform()->getRb()->getBtRigidBody();

				if (body_1 == left_wheel && body_0 == m_plat || body_0 == left_wheel && body_1 == m_plat)
				{
					this->getMovablePlatform()->movePlatform();
				}


				btRigidBody *other_floor = this->destination_floor->getRb()->getBtRigidBody(); 

				if (body_1 == other_floor && body_0 == m_plat || body_0 == other_floor && body_1 == m_plat)
				{
					this->getMovablePlatform()->stopMovement(); 
				}
			}
		}


	}

}
