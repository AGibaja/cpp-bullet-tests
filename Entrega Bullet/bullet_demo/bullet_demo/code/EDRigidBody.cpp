#include <EDRigidBody.hpp>
#include <EShape.hpp>


EDRigidBody::EDRigidBody(std::shared_ptr<EShape> shape, float mass, btVector3 position, btVector3 rotation)
	: ERigidBody(shape, position, rotation), mass(btScalar(mass))
{
	this->initBody();
}


EDRigidBody::~EDRigidBody()
{
}


void EDRigidBody::initBody()
{
	btVector3 local_inertia(0, 0, 0);

	shape->getBtShape().get()->calculateLocalInertia(mass, local_inertia);

	btRigidBody::btRigidBodyConstructionInfo b
	(
		mass, this->motion_state.get(), this->shape->getBtShape().get(), local_inertia
	);

	this->rigid_body = new btRigidBody(b);
}