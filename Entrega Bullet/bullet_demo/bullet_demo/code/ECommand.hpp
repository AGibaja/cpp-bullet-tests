#ifndef ECOMMAND_HPP
#define ECOMMAND_HPP


class ECommand
{
public: 
	virtual void run() = 0;
};


#endif // !ECOMMAND_HPP
