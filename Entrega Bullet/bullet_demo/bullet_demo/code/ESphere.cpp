#include <ESphere.hpp>


ESphere::ESphere(float radius)
	: radius(radius)
{
	this->bt_shape = std::make_shared<btSphereShape>(radius);
	this->shape_type = SPHERE;
}


float ESphere::getRadius()
{
	return this->radius;
}