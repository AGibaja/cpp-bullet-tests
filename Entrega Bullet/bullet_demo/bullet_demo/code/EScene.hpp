/*!
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

/*!
 * @class EScene
 * @brief Encapsulation of the general scene.
 * This class contains a "graphics world" which is the scene with its scene graphs used in opengl.
 * It also contains an encapsulation of a "physics world", which is fundamentaly bullets world 
 * wrapped up with some update functions and some setters / getters.
 * It also stores references to important elements in the scene, mainlly entities, so we can
 * avoid using casts in execution time.
 */



#pragma once
#ifndef ESCENE_HPP
#define ESCENE_HPP


#include <Updateable.hpp>
#include <Render_Node.hpp>
#include <Model_Obj.hpp>
#include <btBulletDynamicsCommon.h>
#include <Light.hpp>
#include <EPhysicsWorld.hpp>
#include <EPlatform.hpp>

using RenderNode = glt::Render_Node;

class ECatapult;
class EKey;
class EDoor;
class EDPlatform;


class EScene : public Updateable
{
public:
	///Default constructor for the scene.
	EScene();


	///Constructor that receives a physics world and a render node (root render node).
	/*!
	 * @param p_world own abstraction of physics world.
	 * @param g_world root node of opengl toolkit to draw what is needed.
	 */
	EScene(std::shared_ptr<EPhysicsWorld> p_world,
		std::shared_ptr<RenderNode> g_world);


	///Constructor that receives a physics world, render node and entities.
	/*!
	 * @param p_world own abstraction of physics world.
	 * @param g_world root node of opengl toolkit to draw what is needed.
	 * @param entities vector of entities to fill the world.
	 */
	EScene(std::shared_ptr<EPhysicsWorld> p_world,
		std::shared_ptr<RenderNode>  g_world,
		   std::vector<std::shared_ptr<EEntity>> entities);
	

	///Step the world, check for collsions and update all EEntity instances.
	void update() override;


	///Render root node witrh scene graph.
	void render();


	///Set root node of opengl toolkit scene graph.
	/*!
		@param graphics_scene Graphics part of the scene.
	*/
	void setGraphicsScene(std::shared_ptr<RenderNode> graphics_scene);


	///Set entities from vector.
	/*!
		@param entities Vector of entities that exist in this scene.
	*/
	void setEntities(std::vector<std::shared_ptr<EEntity>> entities);


	///Set camera as a render node with position and rotation.
	/*!
		@param position Position of the camera.
		@param rotation Rotation of the camera.
	*/
	void setCamera(glt::Vector3 position, 
		 glt::Vector3 rotation = glt::Vector3(0, 0, 0)); 


	///Set camera from other pointer to camera.
	/*!
		@param camera Camera to set for the scene.
	*/
	void setCamera(std::shared_ptr<glt::Camera> camera); 


	///Setter for the light from an opengltoolkit light.
	/*!
		@param light Source light for the scene.
	*/
	void setLight(std::shared_ptr<glt::Light> light);


	/*!
		@brief Add an entity with a name.
		@details
			This function recieves an entity, adds its attached rigidbody to the bullet world,
			a render node with a given name to the root render node of opengltoolkit, and finally
			adds the entity received as a parameter to the entities vector.
		
	   @param name name to add it with to the scene.
	   @param e pointer to the entity to add to the entities vector.
	 */
	void addEntity(std::string name, std::shared_ptr<EEntity> e);
	
	
	///Setter for the catapult entity.
	/*!
		@param catapult Pointer to the catapult of the scene.
	*/
	void setCatapult(std::shared_ptr<ECatapult> catapult);


	///Setter for the key entity.
	/*!
		@param key Pointer to the key of the scene.
	*/
	void setKey(std::shared_ptr<EKey> key);


	///Setter for the door entity.
	/*!
		@param door Pointer to the door of the scene.
	*/
	void setDoor(std::shared_ptr<EDoor> door);


	///Setter for the movable platofrom entity.
	/*!
		@param platform Platform of the scene.
	*/
	void setMovablePlatform(std::shared_ptr<EDPlatform> platform);


	///Setter for the destination floor entity.
	/*!
		@details The movable floor will keep going until it collides with the platform we tell it.
		@param destiny What platform does the movable floor have to hit to stop?
	*/
	void setDestinationFloor(std::shared_ptr<EPlatform> destiny);


	///Getter for the renders scene root node.
	std::shared_ptr<RenderNode> getGraphicsWorld();


	///Getter for the camera. 
	std::shared_ptr<glt::Camera> getCamera();


	///Setter for the opengl toolkit light.
	std::shared_ptr<glt::Light> getLight();

	///Getter for the physics world. 
	std::shared_ptr<EPhysicsWorld> getPhysicsWorld();


	///Getter for the catapult. 
	std::shared_ptr<ECatapult> getCatapult(); 


	///Getter for the movable platform (where the catapult has to be to get to the other side). 
	std::shared_ptr<EDPlatform> getMovablePlatform();


	///Getter for the other floor where the catapult has to go. 
	std::shared_ptr<EPlatform> getDestinationPlatform();


	///Getter for the door that opens. 
	std::shared_ptr<EDoor> getDoor();


	///Getter for the entities vector. 
	std::vector<std::shared_ptr<EEntity>> getEntities();
	

private: 
	std::shared_ptr<RenderNode> graphics_world;

	std::shared_ptr<glt::Model_Obj> m = nullptr; 
	std::shared_ptr<glt::Camera> camera = nullptr; 
	std::shared_ptr<EPhysicsWorld> physics_world = nullptr;
	std::shared_ptr<ECatapult> catapult = nullptr;
	std::shared_ptr<EDPlatform> movable_platform = nullptr;
	std::shared_ptr<EPlatform> destination_floor = nullptr;
	std::shared_ptr<glt::Light> light = nullptr;
	std::shared_ptr<EKey> key = nullptr;
	std::shared_ptr<EDoor> door = nullptr;

	std::vector<std::shared_ptr<EEntity>> entities; 


	///Initialize some properties needed for the scene.
	void initScene(); 


	///Set some scene elements.
	void setSceneElements();


	///Check collisions between various objects.
	/*!
	 * Checks collsision between some pairs of objects. 
	 * It checks collsions with: 
	 * --> The catapult body and the key, in order to interact with said key. 
	 * --> The catapults left wheel and the movable platform, so the platform 
	 *     knows when to start moving to the other side. 
	 * --> The movable platform and the platform at the other side, so it knows when to 
	 *     stop moving, and it doesn't bounce or has undesired behaviour.
  	 */
	void checkCollisions(); 

};


#endif // !ESCENE_HPP
