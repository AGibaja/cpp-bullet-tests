/*!
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

/*!
	@class EDoor
	@brief Abstraction of a door entity into own class.
	@details If the key is picked, the door will open.
*/



#ifndef EDOOR_HPP
#define EDOOR_HPP

#include <EPlatform.hpp>


class EDoor : public EEntity
{
public: 
	///Constructor of the door, that initializes the door in the scene.
	/*!
	 * @param position origin of the doors transform
	 * @param scale scale of the doors transform
	 * @param rotation rotation  of the doors transform
	 */
	EDoor(btVector3 position = btVector3(0, 2.f, -15.f), btVector3 scale = btVector3(10.f, 1, 10),
		btVector3 rotation = btVector3(0, 90, 0));

	///Raises this door.
	void openDoor();


private:
	///Is the door open?
	bool is_open = false; 

	///Initializes the entities properties as needed.
	/*!
	 * Initializes the doors graphics, physics (rigidbodies) and shapes.
	 */
	void initDoor(); 

};

#endif