#include <ECatapult.hpp>
#include <EDRigidBody.hpp>
#include <ECylinder.hpp>
#include <EScene.hpp>
#include <EWheel.hpp>


ECatapult::ECatapult(EScene *s, btVector3 position, btVector3 scale, btVector3 rotation)
	: EEntity(position, scale, rotation), s(s)
{
	this->graphics = std::make_shared<ModelObj>("../../../../assets/box.obj");
	this->shape = std::make_shared<EBox>(this->scale*0.5f);
	this->rigid_body = std::make_shared<EDRigidBody>(this->shape, 0.25f, position, rotation);
	this->rigid_body->getBtRigidBody()->setAngularFactor(btVector3(0, 1, 0));
	btScalar x_pos = position.x() - 1;
	btScalar y_pos = position.y();

	btVector3 offset_1 = btVector3(x_pos, y_pos, position.z());
	btVector3 scale_1 = btVector3(0.5f, 0.25f, 0.5f);
	btVector3 rotation_1 = btVector3(90.f, 90, 0.f);
	std::shared_ptr<EWheel> w = std::make_shared<EWheel>(this, offset_1, scale_1, rotation_1);
	w->getRb()->getBtRigidBody()->setActivationState(DISABLE_DEACTIVATION);
	
	this->s->addEntity("Wheel1", w);


	btScalar x_pos_2 = position.x() + 1;
	btScalar y_pos_2 = position.y();
	btVector3 offset_2 = btVector3(x_pos_2, y_pos_2, position.z());
	btVector3 scale_2 = btVector3(0.5f, 0.25f, 0.5f);
	btVector3 rotation_2 = btVector3(90.f, 90, 0.f);
	std::shared_ptr<EWheel> w2 = std::make_shared<EWheel>(this, offset_2, scale_2, rotation_2);
	w2->getRb()->getBtRigidBody()->setActivationState(DISABLE_DEACTIVATION);

	this->s->addEntity("Wheel2", w2);


	this->addLeftWheel(w); 
	this->addRightWheel(w2); 
}


ECatapult::~ECatapult()
{
	//delete this->s; 
}


void ECatapult::addLeftWheel(std::shared_ptr<EWheel> wheel)
{
	btHingeConstraint* left_wheel; 
	auto catapult_chasis = this->getRb()->getBtRigidBody();
	auto wheel_body = wheel->getRb()->getBtRigidBody();
	auto chasis_position = btVector3(0, -0.5f, 0.f);
	auto wheel_position = btVector3(0.0f, 1.f, 0.0f);

	left_wheel = new btHingeConstraint(*catapult_chasis, *wheel_body, chasis_position, wheel_position,
		btVector3(0, 0, 0), btVector3(0.f, 0.5f , 0.0), true);
	left_wheel->enableAngularMotor(true, 0.f, 10.f);


	this->s->getPhysicsWorld()->getBulletWorld()->addConstraint(left_wheel);
	this->left_wheel_constraint.reset(left_wheel);
	this->wheels.push_back(wheel);
}



void ECatapult::addRightWheel(std::shared_ptr<EWheel> wheel)
{
	btHingeConstraint* h;
	auto catapult_chasis = this->getRb()->getBtRigidBody();
	auto wheel_body = wheel->getRb()->getBtRigidBody();
	auto chasis_position = btVector3(0, -0.5f, 0.f);
	auto wheel_position = btVector3(0,  -1.f, 0.f);

	h = new btHingeConstraint(*catapult_chasis, *wheel_body, chasis_position, wheel_position,
		btVector3(0.f, 0, 0), btVector3(0, 0.5f, 0), true);
	h->enableAngularMotor(true, 0.f, 10.f);
	this->s->getPhysicsWorld()->getBulletWorld()->addConstraint(h);
	this->right_wheel_constraint.reset(h);
	this->wheels.push_back(wheel);
}


void ECatapult::turnLeft()
{
	this->left_wheel_constraint->setMotorTargetVelocity(-5.f);
	this->right_wheel_constraint->setMotorTargetVelocity(5.f);

	
}


void ECatapult::turnRight()
{
	this->left_wheel_constraint->setMotorTargetVelocity(5.f);
	this->right_wheel_constraint->setMotorTargetVelocity(-5.f);

}


void ECatapult::forward()
{
	this->left_wheel_constraint->setMotorTargetVelocity(5.f);
	this->right_wheel_constraint->setMotorTargetVelocity(5.f);

}

void ECatapult::backwards()
{
	this->left_wheel_constraint->setMotorTargetVelocity(5.f);
	this->right_wheel_constraint->setMotorTargetVelocity(5.f);
}


std::shared_ptr<btHingeConstraint> ECatapult::getLeftWheel()
{
	return this->left_wheel_constraint;
}


std::shared_ptr<btHingeConstraint> ECatapult::getRightWheel()
{
	return this->right_wheel_constraint;
}

