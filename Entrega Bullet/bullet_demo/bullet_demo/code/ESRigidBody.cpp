#include <ESRigidBody.hpp>
#include <EShape.hpp>


ESRigidBody::ESRigidBody(std::shared_ptr<EShape> shape, btVector3 position, btVector3 rotation)
	: EDRigidBody(shape, 0, position, rotation)
{
	this->initBody();
}
