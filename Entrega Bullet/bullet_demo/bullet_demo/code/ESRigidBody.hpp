/*!
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

/*!
	@class ESRigidBody
	@brief Defines own static rigidbody encapsulation.
*/


#ifndef ESRIGIDBODY_HPP
#define ESRIGIDBODY_HPP

#include <EDRigidBody.hpp>


///Static rigidbody implementation.
class ESRigidBody : public EDRigidBody
{
public:
	///Constructor to build a static rigidbody from its shape, a position and a rotation.
	/*!
	 * @param shape pointer to own encapsulation of shape.
	 * @param position position of the rigidbody.
	 * @param rotation rotation of the rigidbody.
 	 */
	ESRigidBody(std::shared_ptr<EShape> shape, btVector3 position, btVector3 rotation);

};

#endif