/*!
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/


/*!
	@class ECylinder
	@brief Abstraction of bullets cylinder shape into own class.
*/

#ifndef ECYLINDER_HPP
#define ECYLINDER_HPP

#include <EBox.hpp>


class ECylinder : public EShape
{
public:
	///Constructor that takes the cylinders dimensions as half extents.
	/*!
		@param dimensions Vector3 with the dimensions of the wheel as half extents.
	*/
	ECylinder(btVector3 dimensions);


	///Getter for the box cylinders dimensions.
	btVector3 getDimensions(); 


private:
	///Dimensions for the wheel as half extents.
	btVector3 dimensions;


};

#endif