#include <EEntity.hpp>
#include <ERigidBody.hpp>
#include <EScene.hpp>
#include <EShape.hpp>




EEntity::EEntity(btVector3 position, btVector3 scale, btVector3 rotation)
	: position(position), scale(scale), rotation(rotation)
{

}


EEntity::EEntity(std::string model_path, std::shared_ptr<ERigidBody> rb, btVector3 scale)
	: path(model_path), rigid_body(rb), scale(scale)
{
	this->graphics = std::make_shared<ModelObj>(path);
}


void EEntity::update()
{
	this->rigid_body->getBtRigidBody()->getMotionState()->getWorldTransform(this->p_transform);
	this->p_transform.getOpenGLMatrix(glm::value_ptr(graphics_transform));

	this->graphics->set_transformation(graphics_transform);
	this->graphics->scale(this->scale.x(), this->scale.y(), this->scale.z());
}


std::string EEntity::getPath()
{
	return this->path;
}


btVector3 EEntity::getScale()
{
	return this->scale;
}


btTransform EEntity::getPTransform()
{
	return this->p_transform;
}


std::shared_ptr<ERigidBody> EEntity::getRb()
{
	return this->rigid_body;
}


std::shared_ptr<ModelObj> EEntity::getGraphics()
{
	return this->graphics; 
}