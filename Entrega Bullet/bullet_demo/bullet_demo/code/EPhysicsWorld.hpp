/**
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

///Represents a key that raises the door of the scene.
/*!
	@class EPhysicsWorld
	@brief Own encapsulation of the physics world into an own class.
	@details The physics world is an interface so elements of the physics world can be properly managed.
*/



#pragma once
#ifndef EPHYSICSWORLD_HPP
#define EPHYSICSWORLD_HPP

#include <vector>
#include <memory>
#include <btBulletDynamicsCommon.h>



class EScene;

class EPhysicsWorld
{
public:
	///Constructtor that initializes the bullet world with a gravity vector.
	/*!
		@param gravity Gravity vector of the world.
	*/
	EPhysicsWorld::EPhysicsWorld(btVector3 gravity = btVector3(0, -10, 0));
	
	///Destructor of the physics world, releases the bullet world pointer to avoid double deletes and errors on cleanup.
	~EPhysicsWorld();

	///Get the gravity vector of this world.
	btVector3 getGravity(); 

	///Get a pointer to the discrete dynamics world of bullet.
	btDiscreteDynamicsWorld* getBulletWorld();


private:
	///Gravity vector.
	btVector3 gravity;

	///Pointer to the bullet world.
	std::unique_ptr<btDiscreteDynamicsWorld> bullet_world;

	///Collsition configuration for the bullet world.
	btDefaultCollisionConfiguration collision_configuration;
	///Collision dispatcher for the bullet world.
	btCollisionDispatcher collision_dispatcher;	
	///Broad phase solver for the bullet world.
	btDbvtBroadphase broad_phase;
	///Constraint solver for the bullet world.
	btSequentialImpulseConstraintSolver constraint_solver;			

	///Initialize the bullet world.
	void initBulletWorld(); 


};


#endif // !EPHYSICSWORLD_HPP
