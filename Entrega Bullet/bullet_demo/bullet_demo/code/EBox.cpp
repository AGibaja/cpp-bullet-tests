#include <EBox.hpp>


EBox::EBox(btVector3 dimensions)
	: dimensions(dimensions)
{
	this->bt_shape = std::make_shared<btBoxShape>(dimensions);
	this->shape_type = BOX;
}


btVector3 EBox::getDimensions()
{
	return this->dimensions;
}