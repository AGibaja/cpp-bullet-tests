/*!
	Author �lvaro Gibaja Mohamed
	Date: 6-19-2019
*/

/*!
	@class EDPlatform
	Abstraction of a platform that moves into own class. The D on EDPlatform
	stands for dynamic.
*/




#ifndef EDPLATFORM_HPP
#define EDPLATFORM_HPP

#include <EPlatform.hpp>

class EDPlatform : public EPlatform
{
public: 
	///Constructor of the platform.
	/*!
	 * @param position origin of the platform transform
	 * @param scale scale of the platform transform
	 * @param rotation rotation  of the platform transform
	 */
	EDPlatform(btVector3 position = btVector3(0, -2.5f, -20.f), btVector3 scale = btVector3(5.f, 1.f, 5.f),
		btVector3 rotation = btVector3(0, 0, 0));


	///Set the movements platform state.
	/*!
		@param is_moving Moving state for this door.
	*/
	void isMoving(bool is_moving);

	
	///Move the platform until it collides with the destination floor (floor to where it travles).
	void movePlatform();


	///Stop moving the platform.
	void stopMovement(); 


	///Is the platform moving ?
	bool isMoving();


private:
	///Is the door moving?
	bool is_moving = true; 


	///Initializes the entities properties as needed.
	/*!
	 * Initializes the platforms graphics, physics (rigidbodies) and shapes.
	 */
	void initDPlatform();
	

};


#endif // !define EDPLATFORM_HPP
