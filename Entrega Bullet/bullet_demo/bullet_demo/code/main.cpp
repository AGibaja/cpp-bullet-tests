﻿#include <chrono>
#include <thread>
#include <future>

#include <SFML/Window.hpp>

#include <btBulletDynamicsCommon.h>

#include <EScene.hpp>
#include <Cube.hpp>
#include <EDRigidBody.hpp>
#include <EBox.hpp>
#include <ECatapult.hpp>
#include <EKey.hpp>
#include <EDoor.hpp>
#include <EDPlatform.hpp>



using namespace std;
using namespace glt;


static std::shared_ptr<EScene> s;

void reset_viewport(const sf::Window & window, Render_Node & scene)
{
	GLsizei width = GLsizei(window.getSize().x);
	GLsizei height = GLsizei(window.getSize().y);

	scene.get_active_camera()->set_aspect_ratio(float(width) / height);

	glViewport(0, 0, width, height);
}


void createCatapult()
{
	std::shared_ptr<ECatapult> catapult =
		std::make_shared<ECatapult>(s.get(), btVector3(0, 3, -10),
			btVector3(0.5f, 0.25f, 1.f));
	s->addEntity("Catapult", catapult);
	s->setCatapult(catapult);
}


void createFloor()
{

	std::shared_ptr<EPlatform> ground1 =
		std::make_shared<EPlatform>(btVector3(0, -2, -10), btVector3(15, 0.5f, 15), btVector3(0, 0, 0));
	s->addEntity("Ground1", ground1);


	std::shared_ptr<EPlatform> ground2 = 
		std::make_shared<EPlatform>(btVector3(0, -3.05f, -40), btVector3(15, 0.5f, 15), btVector3(0, 0, 0));
	s->addEntity("Ground2", ground2);
	s->setDestinationFloor(ground2);
}


void createDoor()
{
	std::shared_ptr<EDoor> door =
		std::make_shared<EDoor>();
	s->addEntity("Door", door);
	s->setDoor(door);
}


void createKey()
{
	std::shared_ptr<EKey> key = std::make_shared<EKey>(btVector3(0, -1, -5),
		btVector3(0.5f, 0.25f, 3.0f));
	s->addEntity("Key", key);
	
	s->setKey(key);
}


void createPlatform()
{
	std::shared_ptr<EDPlatform> platform = std::make_shared<EDPlatform>(); 
	s->addEntity("Movable Platform", platform);
	s->setMovablePlatform(platform);
}


int main(int argc, char **argv)
{

	
	sf::Window window
	(
		sf::VideoMode(1024, 720),
		"Bullet Rigid Bodies",
		sf::Style::Default,
		sf::ContextSettings(24, 0, 0, 3, 2, sf::ContextSettings::Core)
	);


	if (!glt::initialize_opengl_extensions())
	{
		exit(-1);
	}

	window.setVerticalSyncEnabled(true);


	s = std::make_shared<EScene>();
	reset_viewport(window, *s->getGraphicsWorld());
	
	
	createFloor(); 
	createDoor();
	createCatapult();
	createKey();
	createPlatform();


	glClearColor(0.2f, 0.2f, 0.2f, 1.f);

	bool running = true;
	int  frame = 0;

	do
	{
		sf::Event event;

		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
			{
				running = false;
				break;
			}

			case sf::Event::Resized:
			{
				break;
			}

			case sf::Event::KeyPressed:
			{
				switch (event.key.code)
				{
					case (sf::Keyboard::Key::Left):
						s->getCamera()->translate(glt::Vector3(1, 0, 0));
							break; 

					case (sf::Keyboard::Key::Right):
						s->getCamera()->translate(glt::Vector3(-1, 0, 0));
						break;

					case (sf::Keyboard::Key::Up):
						s->getCamera()->translate(glt::Vector3(0, 1, 0));
						break;


					case (sf::Keyboard::Key::Down):
						s->getCamera()->translate(glt::Vector3(0, -1, 0));
						break;


					case (sf::Keyboard::Key::R):
						s->getCamera()->rotate_around_x(0.25f);
						break;


					case (sf::Keyboard::Key::E):
						s->getCamera()->rotate_around_x(-0.25f);
						break;


					case (sf::Keyboard::Key::Add):
						s->getCamera()->translate(glt::Vector3(0, 0, -1));
						break;


					case (sf::Keyboard::Key::Subtract):
						s->getCamera()->translate(glt::Vector3(0, 0, 1));
						break;


					case (sf::Keyboard::Key::W):
						s->getCatapult()->forward(); 
						break;

					case (sf::Keyboard::Key::A):
						s->getCatapult()->turnLeft(); 
						break;


					case (sf::Keyboard::Key::D):
						s->getCatapult()->turnRight();
						break;

					case (sf::Keyboard::Key::L):
						s->getMovablePlatform()->movePlatform(); 
						break;

				}
				break;
			}

			case sf::Event::KeyReleased:
			{
				s->getCatapult()->getLeftWheel()->setMotorTargetVelocity(0);
				s->getCatapult()->getRightWheel()->setMotorTargetVelocity(0);
				break;
			}

			case sf::Event::MouseButtonReleased:
			{
				break;
			}
			}
		}

		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		s->update();
		s->render();
		
		window.display();
	} while (running);

	return EXIT_SUCCESS;
}